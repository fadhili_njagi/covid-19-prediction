import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LogisticRegression
from sklearn.impute import SimpleImputer

print("This is a program that predicts whether or not a patient has COVID-19 based on their symptoms")
print("Please Enter your symptoms below\n")
cough = input("Are you coughing? (y/n): ")
fever = input("Do you have a fever? (y/n): ")
sore_throat = input("Do you have a sore_throat? (y/n): ")
shortness_of_breath = input("Are you experiencing shortness of breath? (y/n): ")
head_ache = input("Do you have a headache? (y/n): ")
age_60_and_above = input("Are you 60 years old and above? (y/n): ")
travelled_abroad = input("Have you travelled abroad recently? (y/n): ")
contact_with_confirmed = input("Have you been in contact with a confirmed COVID-19 patient? (y/n): ")
gender = input("Are you male or female? (m/f): ")
patient = np.empty(dtype='int')

if cough not in ["y", "n"]:
    raise Exception ("Only y or n values allowed")
else
    patient = np.append(patient, [1 if cough == 'y' else 0])

if fever not in ["y", "n"]:
    raise Exception ("Only y or n values allowed")
else
    patient = np.append(patient, [1 if fever == 'y' else 0])

if sore_throat not in ["y", "n"]:
    raise Exception ("Only y or n values allowed")
else
    patient = np.append(patient, [1 if sore_throat == 'y' else 0])

if shortness_of_breath not in ["y", "n"]:
    raise Exception ("Only y or n values allowed")
else
    patient = np.append(patient, [1 if shortness_of_breath == 'y' else 0])

if head_ache not in ["y", "n"]:
    raise Exception ("Only y or n values allowed")
else
    patient = np.append(patient, [1 if head_ache == 'y' else 0])

if age_60_and_above not in ["y", "n"]:
    raise Exception ("Only y or n values allowed")
else
    patient = np.append(patient, [1 if age_60_and_above == 'y' else 0])

if travelled_abroad not in ["y", "n"]:
    raise Exception ("Only y or n values allowed")
else
    patient = np.append(patient, [1 if travelled_abroad == 'y' else 0])

if contact_with_confirmed not in ["y", "n"]:
    raise Exception ("Only y or n values allowed")
else
    patient = np.append(patient, [1 if contact_with_confirmed == 'y' else 0])

if gender not in ["m", "f"]:
    raise Exception ("Only m or f values allowed for gender")
else
    patient = np.append(patient, [1 if gender == 'm' else 0])
    patient = np.append(patient, [1 if gender == 'f' else 0])

df = pd.read_csv("Cleaned COVID 19 Tests dataset.csv", dtype="int")
Y = df['corona_result'].values
df = df.drop('corona_result', axis=1)
X = df.values
model = LogisticRegression()
model.fit(X, Y)
prediction = model.predict(patient[[0]])
if prediction == 1:
    print("You probably have COVID-19. You should get tested or self-isolate")
else
    print("You don't seem to have COVID-19")